'use strict';
const logger = require('../commonlib/logger');

exports.register = function(server, options, next) {
  server.on('response', function(request) {
    logger.accessLog(request);
  });
  return next();
};

exports.register.attributes = {
  name: 'access-log',
  version: '0.1.0',
};
/*
  常用資料說明
  request.id      : unique request id
  request.method  : http request method
  request.mime    : http request mime type
  request.headers : request headers
  request.params  : router rules parameters
  request.query   : get method parameters (the key-value part of the URI between '?' and '#')
  request.payload : post method parameters

  request.info               : about client
  request.info.received      : received timestamp
  request.info.host          : reqeust host (ex: localhost:8000)
  request.info.hostname      : request hostname (ex: localhost)
  request.info.remoteAddress : client ip
  request.info.remotePort    : client port
  request.info.referrer      : client referrer

  request.url      : about request url
  request.url.path : request url path

  request.response                   : about server response
  request.response.variety           : response type
     'file': static file
     'view': reply.view()
     'plain': reply()
  request.response.statusCode        : no error, and response code
  request.response.source            : server response data
  request.response.isBoom            : has error
  request.response.output.statusCode : response error code when isBoom=true
  request.response.source.template   : user view template when variety=view
  request.response.source.settings.compileOptions.filename : template name

  request.route        : about route
  request.route.method : 'get'
  request.route.path   : route rules path
*/