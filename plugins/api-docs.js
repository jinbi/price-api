'use strict';

var hapiSwaggered = {
    register: require('hapi-swaggered'),
    options: {
        stripPrefix: "/apis",
        tagging: {
            mode: 'path', //依路徑分群組
            //pathLevel: 1,  //路徑分組時的層次
            stripRequiredTags: false
        },

        tags: {
            'todos/v1': 'Sample todo description',
        },
    },
};

var hapiSwaggeredUi = {
    register: require('hapi-swaggered-ui'),
    path: '/apis/docs',
    options: {
        title: 'Deepblu.Social',
        authorization: {
            field: 'authorization',
            scope: 'query', // header works as well
            // valuePrefix: 'bearer '// prefix incase
            defaultValue: 'demoKey',
            placeholder: 'Enter your apiKey here'
        }
    },
};

exports.register = function (server, options, next) {
    server.register([
        hapiSwaggered,
        hapiSwaggeredUi
    ], {
        routes: {
            prefix: '/apis/docs',
        }
    }, function (err) {
        if (err) {
            throw err;
        }
    });
    return next();
};

exports.register.attributes = {
    name: 'social-docs',
    version: '1.0',
};
