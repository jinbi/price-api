'use strict';

const Config = require('config');

let plugins = [
    require('inert'),
    require('vision'),
    {
        register: require('hapi-router'),
        options: {
            routes: 'controllers/**/*router.js' // uses glob to include files
        },
    },
    /*  {
     register: require('yar'), // session
     options: {
     storeBlank: false,
     name: 'sessionid',
     maxCookieSize: 0, //set zero , always save to redis
     cache: {
     cache: 'redisCache',
     segment: 'session',
     expiresIn: Config.get('Session.expire'), // 4 * 60 * 60 * 1000 = 4 hr
     },
     cookieOptions: {
     password: Config.get('Session.secret'),
     isSecure: Config.get('Session.isSecret'), //need https
     isHttpOnly: Config.get('Session.isHttpOnly'),
     path: '/',
     ttl: Config.get('Session.ttl'),
     },
     },
     },
     */
    require('./exception-handle'),
    require('./access-log'),
];

//swagger
if (Config.get('Environment') !== 'production') {
    plugins.push({
        register: require('hapi-swagger'),
        options: {
            info: {
                title: 'IP-Backend API',
                version: '1.0.0',
                description: 'for internal portal using apis',
                termsOfService: 'http://deepblu.com',
            },
            jsonPath: '/ipbackend/swagger.json',
            basePath: '/ipbackend/',
            pathPrefixSize: 2,
            swaggerUIPath:'/ipbackend/swaggerUI/',
            documentationPath: '/ipbackend/docs',
            jsonEditor: false,
            expanded: 'none',
            tags: [],
        },
    });
}

//i18n
let i18n = {
    register: require('./i18n.js'),
    options: {}
}
if (Config.get('Environment') === 'development') {
    i18n.options.autoReload = true;
}
plugins.push(i18n);

module.exports = plugins;
