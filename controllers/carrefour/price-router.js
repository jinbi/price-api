/**
 * 2017.12
 * titus
 */

const PriceConterller = require('./price-controller')
const priceConterller = new PriceConterller()
const pricevalidator = require('./price-validator')

const rootPath = '/price'

module.exports = [
  {
    path: rootPath,
    method: 'GET',
    config: {
      validate: pricevalidator.request.fetchPrices,
      handler: (request, reply) => {
        return priceConterller.fetchPrices(request, reply)
      },
      description: 'get prices for all products',
      tags: ['api']
    }
  },
  {
    path: rootPath + '/single',
    method: 'GET',
    config: {
      validate: pricevalidator.request.fetchOnePrice,
      handler: (request, reply) => {
        return priceConterller.fetchOnePrice(request, reply)
      },
      description: 'get single product price',
      tags: ['api']
    }
  },
  {
    path: rootPath,
    method: 'POST',
    config: {
      validate: pricevalidator.request.savePrices,
      handler: (request, reply) => {
        return priceConterller.savePrices(request, reply)
      },
      description: 'save prices',
      tags: ['api']
    }
  }
]
