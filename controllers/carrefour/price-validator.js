/**
 * titus 2017.12
 */

const Joi = require("joi");
const validatorCommon = require("../../commonlib/validator-common");

var request = {
    savePrices : {
        headers: validatorCommon.headersAllowSkipToken
    },
    fetchPrices : {
        headers: validatorCommon.headersAllowSkipToken
    },
    fetchOnePrice: {
      headers: validatorCommon.headersAllowSkipToken,
      query: {
          productName : Joi.string().required().description('product name')
      }
    }
};

module.exports = {
    request: request
}
