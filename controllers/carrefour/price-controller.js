/**
 * product prices related
 * titus 2017.12
 */

const logTag = '[prices-controller]'
const Logger = require('../../commonlib/logger')
const moment = require('moment')
const PriceModel = require('../../models/carrefour/price-model')

class PriceController {
  constructor () {
  }

  fetchPrices (request, reply) {
    const priceModel = new PriceModel()

    priceModel.dbFetchPrices()
      .then((prices) => {
        return reply.ok(prices)
      })
      .catch((err) => {
        Logger.error(logTag, err)
        return reply.alertMsg('fail')
      })
  }

  fetchOnePrice (request, reply) {
    const priceModel = new PriceModel()
    let productName = request.query.productName

    priceModel
      .dbFetchOnePrice(productName)
      .then((price) => {
        return reply.ok(price)
      })
      .catch((err) => {
        Logger.error(logTag, err)
        return reply.alertMsg('fail')
      })
  }

  savePrices (request, reply) {
    const priceModel = new PriceModel()

    priceModel.dbSavePrices()
      .then((result) => {
        return reply.ok(result)
      })
      .catch((err) => {
        Logger.error(logTag, err)
        return reply.alertMsg('fail')
      })
  }
}

module.exports = PriceController
