const logTag = '[MongoCommon]'
const Logger = require('../../commonlib/logger')

const moment = require('moment')
const axios = require('axios')
const cheerio = require('cheerio')
const Promise = require('bluebird')

let handleE = (e) => {
  return e
}

class PriceModel {
  constructor () {
  }

  dbFetchPrices(){
    const col = MongoConn.db.collection('price')

    return new Promise((resolve, reject) => {
      col
        .find({})
        .toArray()
        .then(results => {
          return resolve(results)
        })
    })
  }

  dbFetchOnePrice(name){
    const col = MongoConn.db.collection('price')

    return new Promise((resolve, reject) => {
      col
        .find({'product': { $regex: name, $options: 'i'}})
        .toArray()
        .then(res => {
          return resolve(res)
        })
    })
  }

  saveIntoMongo (document) {
    const col = MongoConn.db.collection('price')
    return new Promise((resolve, reject) => {
      resolve(col.remove())
    })
    .then( result => {
      col.insertOne(
        {
          product: document.productName,
          price: document.price
        }).then(res => {
          return new Promise.resolve(res)
        })
    })
  }

  dbSavePrices () {
    let uriArray = []
    const domain = 'https://online.carrefour.com.tw/'
    let option = {
      method: 'GET'
    }


    // currently carrefour used product ID to fetch single product price
    // But the design and restricts of the architecture is not know yet
    // Need to use some other packages or implememnt myself to avoid DDOS block
    for (let i = 6000000000000; i > 1000000000000; i--) {
      uriArray.push(domain + i)
    }

    return Promise
      .all(uriArray.map(p => {
        option.url = p
        return axios(option).catch(handleE)
      }))
      .then(results => {
        results.forEach(r => {
          if (r.status == 200) {
            let $ = cheerio.load(r.data)
            let title = $('.pro-name').text()
            let price = $('.pro-price').children()[1].childNodes[2].next.children[0].data

            this
              .saveIntoMongo({productName: title, price: price})
              .then(result => {
                Logger.log(logTag, result)
                return new Promise.resolve(result)
              })
              .catch(err => {
                Logger.error(logTag, err)
                return new Promise.reject(err)
              })
          }
        })
      })
      .catch(err => {
        Logger.error(logTag, err)
      })
  }
}

module.exports = PriceModel
