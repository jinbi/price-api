'use strict'

const Config = require('config')
const Hapi = require('hapi')
const Boom = require('boom')

const Logger = require('./commonlib/logger')
const MongoCommon = require('./commonlib/mongo-common')
const server = new Hapi.Server()

global.MongoConn = new MongoCommon()

Logger.info('load environment config from:', Config.get('Environment') + '.json')

server.connection({
  host: Config.get('Server.host'),
  port: Config.get('Server.port'),
  labels: ['api'],
  app: {
    swagger: {
      info: {
        title: 'price-api',
        description: 'some thing related to price'
      }
    }
  },
  routes: {
    cors: {
      origin: ['*'],
      additionalHeaders: ['authorization', 'accept-language', 'content-type']
    }
        /*
         The Cross-Origin Resource Sharing protocol allows browsers to make cross-origin API calls.
         CORS is required by web applications running inside a browser which are loaded from a different domain than the API server.
         CORS headers are disabled by default (false). To enable, set cors to true, or to an object with the following options:
         */
  }
})

server.decorate('server', 'init', () => {
  return Promise.all([MongoConn.initial()])
            .then(() => {
              return new Promise((resolve, reject) => {
                if (module.parent === null) {
                  server.start(err => {
                    if (err) {
                      reject(err)
                    } else {
                      Logger.info('Server running at:', server.info.uri)
                      resolve()
                    }
                  })
                } else {
                  server.initialize(err => {
                    if (err) {
                      reject(err)
                    } else {
                      resolve(server)
                    }
                  })
                }
              })
            })
            .catch((err) => {
              Logger.error(err)
            })
})

server.decorate('reply', 'ok', function (result) {
  return this.response({
    statusCode: 200,
    message: 'Success',
    result: result
  })
})
server.decorate('reply', 'alertMsg', function (result) {
  Logger.warn(result)
  return this.response(Boom.create(499, result))
})

server.register(require('./plugins'), (err) => {
  if (err) {
    Logger.error('Failed to load plugin:', err)
  } else {
    if (module.parent === null) {
      server.init()
    }
  }
})

server.route({
  method: 'GET',
  path: '/apis/test/{p*}',
  handler: {
    directory: {
      path: 'public'
    }
  }
})

module.exports = server
