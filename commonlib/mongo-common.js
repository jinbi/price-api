
const Promise = require('bluebird');
const Config = require('config');
const MongoClient = require('mongodb').MongoClient
const _ = require('lodash');
const logTag = '[MongoCommon]';
const Logger = require('./logger');

class MongoCommon{
    constructor(option) {
        this.db = {};
    }

    // mongo 初始化
    initial() {
        let that = this;
        let url = Config.get('Mongo.connectionStr');
        Logger.info('connection to Mongo...');

        return new Promise(function(resolve, reject) {
            MongoClient.connect(url, function (err, db) {
                if (err) {
                    Logger.error('mongodb connection failed.', err);
                    reject(err);
                } else {
                    Logger.info('mongodb is ready')
                    that.db = db;
                    resolve();
                }
            });
        });
    };

    // 關閉mongo連線
    shutdown() {
        this.db.close();
    }

    // 斷開重連mongo
    restart() {
        return this.shutdown().then(this.initial.bind(this));
    }

}
module.exports = MongoCommon;
