const Joi = require("joi");
const ValidatorCommon = {};

ValidatorCommon.validatorOk = function(result) {
    return Joi.object({
        statusCode: Joi.number().description('狀態碼'),
        message: Joi.string().description('錯誤訊息'),
        result: result,
    }).label("Result");
}

ValidatorCommon.headers = Joi.object().keys({
    'authorization': Joi.string().required().description('authorization'),
    'accept-language': Joi.string().required().description('device language'),
}).options({allowUnknown: true})

ValidatorCommon.headersAllowSkipToken = Joi.object().keys({
    'authorization': Joi.string().allow('').description('authorization'),
    'accept-language': Joi.string().required().description('device language'),
}).options({allowUnknown: true})

module.exports = ValidatorCommon;
