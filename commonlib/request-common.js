'use strict';

var Config = require('config');
var Boom = require('boom');
var Logger = require('./logger');
var logTag = '[Request-common]';
var UserModel = require('../models/user-model');
var moment = require('moment');

var requestCommon = {};

requestCommon.validateHeader = function(request, reply) {
        let user = {};
        user.language = request.headers['accept-language'];
        user.token = request.headers['authorization'];

        if(user.token === undefined){
            return reply(new Boom.unauthorized('Missing token'));
        }
        console.log('\r\n[path]=', request.url.path)
        console.log('[header]= ', user)
        console.log('[payload]= ', request.payload)
        console.log('[params]= ', request.params)
        console.log('[query]= ', request.query)
        let userModel = new UserModel();

        userModel.dbGetTokenUser(user.token)
            .then(function(res){
                if(res.length > 0){
                    user.ownerId = res[0].userId.toString();
                    var date  = moment(res[0].created).seconds(res[0].ttl).format("X");
                    if(moment().format("X") > date){
                        return reply(Boom.unauthorized(request.i18n.__('TokenExpired')));
                    }else{
                        return reply(user);
                    }
                }else{
                    return reply(Boom.unauthorized(request.i18n.__('InvalidToken')));
                }
            }).catch(function(err){
                Logger.error(logTag, err);
                console.log('invalid token:', err);
            });
};

requestCommon.validateHeaderSkipToken = function(request, reply) {
    let user = {};
    user.language = request.headers['accept-language'];
    user.token = '';
    user.ownerId = '';
    if(request.headers['authorization'] && request.headers['authorization'] != '') {
        return requestCommon.validateHeader(request, reply);
    } else {
        return reply(user);
    }
};

module.exports = requestCommon;
