function utcStampToDatetime() {
    return {$add: [new Date(0), {$add: [{"$multiply": ["$createDTUTC", 1000]}, 8 * 60 * 60 * 1000]} ]};
}

function groupByPeriod(period) {
    switch (period) {
        case 'daily':
            return { year: "$year", month: "$month", day: "$day" }

        case 'weekly':
            return { year: "$year", week: "$week" }

        case 'monthly':
            return { year: "$year", month: "$month" }

        default:
            return { year: "$year", month: "$month", day: "$day" }
    }
}

module.exports = {
    utcStampToDatetime,
    groupByPeriod
}
