'use strict';

/**
 *
 * It's the Winston logs common library.
 *
 * Author: seemo
 * Date: 2016/3/31
 *
 */

const Winston = require('winston');
const moment = require('moment');
const config = require('config');
require('winston-logstash');

//handle timestamp format use localtime
let logTime = () => {
    return moment().format();
};

//處理 logs 的分類
let loadSetting = () => {
    let result = [];
    let consoleLevel = 'error';
    if (config.get('Server.debug') === true) {
        consoleLevel = 'info';
    }
    result.push(
            new (Winston.transports.Console)({
                name: 'console',
                level: consoleLevel,
                colorize: true,
                prettyPrint: true,
            })
    );
    

    //all level setting
    result.push(
        new (require('winston-daily-rotate-file'))({
            name: 'all-file',
            filename: 'logs/winston-all',
            timestamp: logTime,
            datePattern: '.yyyy-MM-dd',
        })
    );
    //debug level setting
    result.push(
        new (require('winston-daily-rotate-file'))({
            name: 'debug-file',
            filename: 'logs/winston-debug',
            level: 'debug',
            timestamp: logTime,
            datePattern: '.yyyy-MM-dd',
        })
    );
    //error level setting (error & info write to same file)
    result.push(
        new (require('winston-daily-rotate-file'))({
            name: 'error-file',
            filename: 'logs/winston-error',
            level: 'error',
            timestamp: logTime,
            datePattern: '.yyyy-MM-dd',
        })
    );
    //info level setting (error & info write to same file)
    result.push(
        new (require('winston-daily-rotate-file'))({
            name: 'info-file',
            filename: 'logs/winston-error',
            level: 'info',
            timestamp: logTime,
            datePattern: '.yyyy-MM-dd',
        })
    );
    return result;
};

//初始化winston
let logger = new Winston.Logger({
    exitOnError: false,
    transports: loadSetting(),
});

if (config.get('LogStash.enable')) {
    //enable logstashTcp logger
    logger.add(Winston.transports.Logstash, {
        name: 'LogStash-tcp',
        port: config.get('LogStash.port'),
        node_name : 'SocialAPI',
        host: config.get('LogStash.host'),
    });
}


logger.format = function (request) {
    let result = {
        'project': config.get('Project'),
        'env': config.get('Environment'),
        'reqId': request.id,
        'received':  moment(request.info.received).format('YYYY-MM-DD HH:mm:ss.SSS [GMT]Z'),
        'path': request.path,
        'method': request.method.toUpperCase(),
        'headers': request.headers,
        'params': request.params,
        'query': request.query,
        'payload': request.payload,
        'token': request.auth.credentials,
        'responded': moment(request.info.responded).format('YYYY-MM-DD HH:mm:ss.SSS [GMT]Z'),
        'response':'',
        'ip': request.info.remoteAddress,
        'statusCode': request.response.statusCode,
        'processTime': request.info.responded - request.info.received
    };
    if(request.response.statusCode !== 200 || config.get('LogStash.logResponse')){
        result.response = JSON.stringify(request.response.source);
    }
    return result;
};

logger.accessLog = function (request) {
    if (request.response.statusCode !== undefined) {
        let log = logger.format(request);
        if (log.statusCode === 200) {
            logger.info(log);
        } else {
            logger.warn(log);
        }
    }
};

logger.exception = function (err) {
    let log = {
        'project': config.get('Project'),
        'env': config.get('Environment'),
        //'reqId': '',
        'received': moment().format('YYYY-MM-DD HH:mm:ss.SSS [GMT]Z'),
        //'path': '',
        //'method': '',
        //'headers': {},
        //'params': {},
        //'query': {},
        //'payload': {},
        //'token': {},
        'responded': moment().format('YYYY-MM-DD HH:mm:ss.SSS [GMT]Z'),
        //'ip': '',
        'statusCode': 500,
        'exception': {},
        //'processTime': ''
    };

    if (err.name) {
        log.exception.name = err.name;
        log.exception.message = err.message;
        log.exception.stack = err.stack.split('\n');
    } else if (err.errName) {
        log.exception.name = err.errName;
        log.exception.message = err.Message;
        log.exception.stack = err.errStack.split('\n');
    }
    logger.error(log);
};

module.exports = logger;


