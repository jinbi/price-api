Only implement to fetch careefour cause the logic would be totally the same for another sales store

使用 mongoDB ^3.6
database : careefour
collections : price
document structure like :
  {
    "_id" : ObjectId("5a407ee47658a909c7ca7789"),
    "product" : "【保久乳】福樂低脂高鈣牛乳(保久乳)200m",
    "price" : "$57"
  }

1. crawler save to mongo :
  router : /price
  method : post
2. fetch all prices:
  router : /price
  method: get
3. fetch single price by product keyword:
  router: /price/single
  method: get
  query param : productName


啟動指令:
  node server.js
